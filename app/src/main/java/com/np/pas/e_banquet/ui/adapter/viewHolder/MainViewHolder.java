package com.np.pas.e_banquet.ui.adapter.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.np.pas.e_banquet.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rashmi on 11/5/2017.
 */

public class MainViewHolder extends RecyclerView.ViewHolder{
    @BindView(R.id.txt_customer_name)
    public TextView txtCustomerName;
    @BindView(R.id.txt_arrival_time)
   public  TextView txtArrivalTime;
    @BindView(R.id.txt_departure_time)
   public TextView txtDepartureTime;
    public MainViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
