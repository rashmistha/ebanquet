package com.np.pas.e_banquet.presenter.pInterface;

/**
 * Created by Android Developer on 11/9/2017.
 */

public interface LoginPresenter extends BasePresenter {
    void onLoginClick(String username, String password);
    void isUserLogin();
}
