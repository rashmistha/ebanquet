package com.np.pas.e_banquet.ui.vInterface;

import android.content.Context;

/**
 * Created by Rashmi on 11/5/2017.
 */

public interface IView {
    void showToast(String message);

    void showLoading();

    void hideLoading();

    void createPresenter();

    Context getContext();
}
