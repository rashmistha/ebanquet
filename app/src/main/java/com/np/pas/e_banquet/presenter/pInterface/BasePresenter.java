package com.np.pas.e_banquet.presenter.pInterface;

import com.np.pas.e_banquet.ui.vInterface.IView;

/**
 * Created by Rashmi on 11/5/2017.
 */

public interface BasePresenter {
    IView getView();
}
