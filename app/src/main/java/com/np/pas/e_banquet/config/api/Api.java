package com.np.pas.e_banquet.config.api;



import com.np.pas.e_banquet.models.apiPojo.UserLoginRequest;
import com.np.pas.e_banquet.models.apiPojo.UserLoginResponse;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Rashmi on 10/18/2017.
 */

public interface Api {
    @POST("Login")
  //  Call<UserLoginResponse> processUserLogin(@Body UserLoginRequest loginResponse);
    Call<UserLoginResponse> processUserLogin(@Body UserLoginRequest loginResponse);


}
