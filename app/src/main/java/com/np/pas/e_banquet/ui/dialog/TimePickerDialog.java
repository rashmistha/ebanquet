package com.np.pas.e_banquet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.widget.TimePicker;

import com.np.pas.e_banquet.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rashmi on 11/5/2017.
 */

public class TimePickerDialog extends Dialog {

    public TimePickerDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timepicker);
        ButterKnife.bind(this);


    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.done)
    public  void btnDone(){
        final TimePicker timePicker1 = (TimePicker) findViewById(R.id.simpleTimePicker1);

//        //int hour1 = timePicker1.getCurrentHour();
//        timePicker1.getHour();
//        timePicker1.getMinute();
//        Toast.makeText(getContext(), "hour: " + timePicker1.getHour() + " & minute: " + timePicker1.getMinute(), Toast.LENGTH_LONG).show();

        dismiss();
    }
}
