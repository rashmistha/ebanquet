package com.np.pas.e_banquet.presenter;


import com.np.pas.e_banquet.presenter.pInterface.BasePresenter;
import com.np.pas.e_banquet.ui.vInterface.IView;

/**
 * Created by ${hemnath} on ${7/31/2017}.
 */

public class BasePresenterImpl<V extends IView> implements BasePresenter {
    private V iView;

    public BasePresenterImpl(V iView) {
        this.iView = iView;
    }

    @Override

    public V getView() {
        return iView;
    }
}
