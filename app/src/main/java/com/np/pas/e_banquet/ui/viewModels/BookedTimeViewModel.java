package com.np.pas.e_banquet.ui.viewModels;

/**
 * Created by Rashmi on 11/6/2017.
 */

public class BookedTimeViewModel {
    String customerName;
    String arrivalTime;
    String departureTime;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }
}
