package com.np.pas.e_banquet.Interactor;

import com.np.pas.e_banquet.Interactor.iInteractor.LoginInteractor;
import com.np.pas.e_banquet.config.api.Api;
import com.np.pas.e_banquet.config.api.RetrofitHelper;
import com.np.pas.e_banquet.models.apiPojo.UserLoginRequest;
import com.np.pas.e_banquet.models.apiPojo.UserLoginResponse;
import com.np.pas.e_banquet.ui.viewModels.UserViewModel;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Android Developer on 11/9/2017.
 */

public class LoginInteractorImpl {

    public void processUserLogin(String userName, String password, final LoginInteractor interactor) {
        final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setUserName(userName);
        loginRequest.setPassword(password);

        RetrofitHelper helper = RetrofitHelper.getInstance();
        Api service = helper.getService(Api.class);

        Call<UserLoginResponse> call = service.processUserLogin(loginRequest);
        call.enqueue(new Callback<UserLoginResponse>() {
            @Override
            public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
               if (response.code() == 200) {
                  UserLoginResponse loginResponse = response.body();
//                    if (loginResponse.getActionStatus().getNumber() == 1) {




                   UserViewModel viewModel = new UserViewModel();
                   viewModel.setIdToken(loginResponse.getIdToken());
                   viewModel.setEmailAddress(loginResponse.getEmailAddress());
                   viewModel.setEmployeeName(loginResponse.getEmployeeName());
                   viewModel.setIdBranch(loginResponse.getIdBranch());
                   viewModel.setIdEmployee(loginResponse.getIdEmployee());

//
                       interactor.onSuccess(viewModel);
//
//                    } else {
//                        interactor.onFailure(loginResponse.getActionStatus().getMessage());
//                    }
              } else {
//                    interactor.onFailure(MessageConstants.LOGIN_ERROR);
              }
            }

            @Override
            public void onFailure(Call<UserLoginResponse> call, Throwable t) {
               // interactor.onFailure("Server Error.");
            }
        });
    }
}
