package com.np.pas.e_banquet.ui.vInterface;

/**
 * Created by Rashmi on 10/15/2017.
 */

public interface IMainView extends IView {
    void setRecycleViewTime();
    void onVenueItem();
    void onClickBooking();
    void onNavigateToLogin();
}
