package com.np.pas.e_banquet.Interactor.iInteractor;


import com.np.pas.e_banquet.ui.viewModels.UserViewModel;

/**
 * Created by Android Developer on 11/9/2017.
 */

public interface LoginInteractor {
    void onSuccess(UserViewModel viewModel);

    void onFailure(String message);
}
