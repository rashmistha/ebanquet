package com.np.pas.e_banquet.presenter;

import com.np.pas.e_banquet.config.api.DbManager;
import com.np.pas.e_banquet.presenter.pInterface.MainActivityPresenter;
import com.np.pas.e_banquet.ui.vInterface.IDialogView;
import com.np.pas.e_banquet.ui.vInterface.IMainView;
import com.np.pas.e_banquet.utils.GlobalUtils;

/**
 * Created by Rashmi on 11/5/2017.
 */

public class MainActivityPresenterImpl extends BasePresenterImpl<IMainView> implements MainActivityPresenter {
    private DbManager  dbManager;

    public MainActivityPresenterImpl(IMainView iView) {
        super(iView);
        this.dbManager=new DbManager();
    }

    @Override
    public void onLogout() {
        GlobalUtils.onShowAlertDialog(getView().getContext(), "Logout", "Are you sure want to logout?", new IDialogView() {
            @Override
            public void onDialogSuccess() {
                dbManager.clearUserSession();
                getView().onNavigateToLogin();
            }
        });
    }
}
