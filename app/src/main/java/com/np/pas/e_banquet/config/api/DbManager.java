package com.np.pas.e_banquet.config.api;

import com.np.pas.e_banquet.models.dbSchema.UserSession;
import com.np.pas.e_banquet.ui.viewModels.UserViewModel;
import com.np.pas.e_banquet.utils.DatabaseUtils;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Android Developer on 11/10/2017.
 */

public class DbManager {
    private Realm realm;

    public DbManager() {
        realm = Realm.getDefaultInstance();
    }

    public void saveUserSession(final UserViewModel viewModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UserSession userSession = DatabaseUtils.getSchemaFromViewModel(viewModel);
                realm.copyToRealmOrUpdate(userSession);
            }
        });
    }

    public void clearUserSession() {
        final RealmResults<UserSession> userSession = realm.where(UserSession.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                userSession.deleteAllFromRealm();
            }
        });
    }
    public UserViewModel getUserSession() {
        UserSession userSession = realm.where(UserSession.class).findFirst();
        return DatabaseUtils.getViewModelFromSchema(userSession);
    }

    public boolean isLogin() {
        UserSession userSession = realm.where(UserSession.class).findFirst();
        return userSession != null;
    }

}
