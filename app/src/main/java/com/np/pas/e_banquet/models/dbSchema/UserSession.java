package com.np.pas.e_banquet.models.dbSchema;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Android Developer on 11/10/2017.
 */

public class UserSession  extends RealmObject {
    @PrimaryKey
    private  int id;
    private String idToken;
    private int idBranch;
    private String emailAddress;
    private  String employeeName;
    private int idEmployee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public int getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(int idBranch) {
        this.idBranch = idBranch;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee) {
        this.idEmployee = idEmployee;
    }
}
