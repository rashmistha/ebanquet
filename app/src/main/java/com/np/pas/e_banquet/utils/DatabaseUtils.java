package com.np.pas.e_banquet.utils;

import com.np.pas.e_banquet.models.dbSchema.UserSession;
import com.np.pas.e_banquet.ui.viewModels.UserViewModel;

/**
 * Created by Android Developer on 11/10/2017.
 */

public class DatabaseUtils {
    public static UserSession getSchemaFromViewModel(UserViewModel viewModel) {
        UserSession userSession = new UserSession();
        userSession.setId(1);
        userSession.setIdToken(viewModel.getIdToken());
        userSession.setIdBranch(viewModel.getIdBranch());
        userSession.setEmailAddress(viewModel.getEmailAddress());
        userSession.setEmployeeName(viewModel.getEmployeeName());
        userSession.setIdEmployee(viewModel.getIdEmployee());

        return userSession;
    }
    public static UserViewModel getViewModelFromSchema(UserSession userSession) {
        UserViewModel viewModel = new UserViewModel();
        viewModel.setIdToken(userSession.getIdToken());
        viewModel.setIdBranch(userSession.getIdBranch());
        viewModel.setEmailAddress(userSession.getEmailAddress());
        viewModel.setEmployeeName(userSession.getEmployeeName());
        viewModel.setIdEmployee(userSession.getIdEmployee());

        return viewModel;
    }
}
