package com.np.pas.e_banquet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.np.pas.e_banquet.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Android Developer on 11/8/2017.
 */

public class TimeDurationDialog extends Dialog {


    public TimeDurationDialog(Context context) {
        super(context);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.time_duration_dialog);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.btn_time_submit)
    void onClickSubmit(){
       dismiss();
    }
    @OnClick(R.id.et_arrival_time)
    void onClickArrivalTime(){
        new TimePickerDialog(getContext()).show();
    }
    @OnClick(R.id.et_departure_time)
    void onClickDepartureTime(){
        new TimePickerDialog(getContext()).show();
    }


}
