package com.np.pas.e_banquet.presenter;

import com.np.pas.e_banquet.Interactor.LoginInteractorImpl;
import com.np.pas.e_banquet.Interactor.iInteractor.LoginInteractor;
import com.np.pas.e_banquet.config.api.DbManager;
import com.np.pas.e_banquet.presenter.pInterface.LoginPresenter;
import com.np.pas.e_banquet.ui.vInterface.ILoginView;
import com.np.pas.e_banquet.ui.viewModels.UserViewModel;
import com.np.pas.e_banquet.utils.NetworkUtils;
import com.np.pas.e_banquet.utils.contants.MessageConstants;

/**
 * Created by Android Developer on 11/9/2017.
 */

public class LoginPresenterImpl extends BasePresenterImpl<ILoginView> implements LoginPresenter {
   private DbManager dbManager;
    public LoginPresenterImpl(ILoginView iView) {
        super(iView);
        this.dbManager = new DbManager();

    }

    @Override
    public void onLoginClick(String username, String password) {
        if (username.isEmpty()) {
            getView().onUserNameError(MessageConstants.EMAIL_ERROR);
            return;
        }
        if (password.isEmpty()) {
            getView().onPasswordError(MessageConstants.PASSWORD_ERROR);
            return;
        }
        if (!NetworkUtils.isConnected(getView().getContext())) {
            NetworkUtils.noInternetConnection(getView().getContext());
            return;
        }
        getView().showLoading();
        LoginInteractorImpl interactor = new LoginInteractorImpl();
        interactor.processUserLogin(username, password,new LoginInteractor() {
            @Override
            public void onSuccess(UserViewModel viewModel) {
                if (getView() != null) {
                    dbManager.saveUserSession(viewModel);
                    getView().showToast(MessageConstants.LOGIN_SUCCESS);
                    getView().onNavigateToMainScreen();
                    getView().hideLoading();
                }
            }

            @Override
            public void onFailure(String message) {
                if (getView() != null) {
                    getView().showToast(message);
                    getView().hideLoading();
                }
            }
        });
    }

    @Override
    public void isUserLogin() {
        if (dbManager.isLogin()) {
            if (getView() != null)
                getView().onNavigateToMainScreen();
        }
    }
}
