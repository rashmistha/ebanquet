package com.np.pas.e_banquet.utils.contants;

/**
 * Created by Rashmi on 10/22/2017.
 */

public class MessageConstants {
    public static final String LOADING = "Loading...";
    public static final String EMAIL_ERROR = "Please enter a valid email.";
    public static final String PASSWORD_ERROR = "Please enter a valid  password.";
    public static final String LOGIN_ERROR = "Email and password incorrect. Please enter a valid email and password.";
    public static final String Networrk_error = "Operation failed, Please Check your internet connection.";

    public static final String LOGIN_SUCCESS = "Login Successfully!";
    public static final String DATA_EMPTY = "Data not found!";
    public static final String DATA_Unauthorized = "Operation denied by server!";

    public static final String PASSWORD_CONFIRM = "Please Enter Same NewPassword and Confirm Password";
    public static final String EOD_Sucess = "EOD Success";
    public static final String EOD_Failure = "EOD Failure";
}
