package com.np.pas.e_banquet.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.np.pas.e_banquet.R;
import com.np.pas.e_banquet.presenter.MainActivityPresenterImpl;
import com.np.pas.e_banquet.presenter.pInterface.MainActivityPresenter;
import com.np.pas.e_banquet.ui.adapter.AllTimeRecycleViewAdapter;
import com.np.pas.e_banquet.ui.coreUi.BaseActivity;
import com.np.pas.e_banquet.ui.dialog.TimeDurationDialog;
import com.np.pas.e_banquet.ui.vInterface.IMainView;
import com.np.pas.e_banquet.ui.viewModels.BookedTimeViewModel;
import com.hornet.dateconverter.DatePicker.DatePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity<MainActivityPresenter> implements IMainView, DatePickerDialog.OnDateSetListener {
    @BindView(R.id.recycleView_all_time)
    RecyclerView recyclerViewAllTime;
    BookedTimeViewModel bookedTimeViewModel;
    private String[] myDataset = null;

    DatePickerDialog datePickerDialog;
    FragmentManager fm;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setRecycleViewTime();
        Calendar now = Calendar.getInstance();

//        Date date = new Date();
//        simpleCalender.setMinDate(date.getTime());
        bookedTimeViewModel = new BookedTimeViewModel();
        datePickerDialog = DatePickerDialog.newInstance(MainActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));


        datePickerDialog.setTitle("DatePicker Title");


        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();

        showCalender();
        //  onDateSet(datePickerDialog,(Calendar.YEAR), (Calendar.MONTH), (Calendar.DAY_OF_MONTH));

    }

    public void showCalender() {
        datePickerDialog.show(getSupportFragmentManager(), "DatePicker");
        ft.replace(R.id.date_fragment_layout, datePickerDialog, null);
        ft.commit();
    }

    @Override
    public void setContext() {
        context = this;
    }

    @Override
    public void createPresenter() {
        presenter = new MainActivityPresenterImpl(this);

    }

    @OnClick(R.id.btn_booking1)
    public void btnBooking() {

        onClickBooking();
    }

    @Override
    public void setRecycleViewTime() {
        BookedTimeViewModel bookedTimeViewModel = new BookedTimeViewModel();

        bookedTimeViewModel.setCustomerName("Madeline Kemedy");
        bookedTimeViewModel.setArrivalTime("9849878790");
        bookedTimeViewModel.setDepartureTime("May 20,2017,10:20am to 12:30pm");
        myDataset = new String[]{bookedTimeViewModel.getCustomerName(), bookedTimeViewModel.getArrivalTime(), bookedTimeViewModel.getDepartureTime(), "", "", ""};
        //  myDataset= new String[]{bookedTimeViewModel.getCustomerName(),bookedTimeViewModel.getArrivalTime(),bookedTimeViewModel.getDepartureTime(),"","",""};


        AllTimeRecycleViewAdapter allTimeRecycleViewAdapter = new AllTimeRecycleViewAdapter(this, this, myDataset);
        recyclerViewAllTime.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAllTime.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAllTime.setAdapter(allTimeRecycleViewAdapter);
    }

    @Override
    public void onVenueItem() {
        Toast.makeText(getContext(), "Item Click", Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout_icon:
                presenter.onLogout();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClickBooking() {
        new TimeDurationDialog(this).show();

    }

    @Override
    public void onNavigateToLogin() {
        Toast.makeText(getApplicationContext(), "Please logout ", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year + "/" + monthOfYear + "/" + dayOfMonth;
        Toast.makeText(MainActivity.this, "CLICKED DATE " + year + "/" + monthOfYear + "/" + dayOfMonth, Toast.LENGTH_SHORT).show();
        Log.d("dayyy", date);
    }
}