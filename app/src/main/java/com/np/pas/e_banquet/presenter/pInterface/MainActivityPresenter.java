package com.np.pas.e_banquet.presenter.pInterface;

/**
 * Created by Rashmi on 11/5/2017.
 */

public interface MainActivityPresenter extends BasePresenter {
    void onLogout();

}
