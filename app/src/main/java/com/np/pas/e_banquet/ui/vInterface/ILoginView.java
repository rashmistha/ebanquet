package com.np.pas.e_banquet.ui.vInterface;

/**
 * Created by Android Developer on 11/9/2017.
 */

public interface ILoginView extends IView {
    void onUserNameError(String msg);

    void onPasswordError(String msg);
    void onNavigateToMainScreen();
}
