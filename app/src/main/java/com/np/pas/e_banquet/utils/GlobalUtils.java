package com.np.pas.e_banquet.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.np.pas.e_banquet.ui.vInterface.IDialogView;

/**
 * Created by Android Developer on 11/9/2017.
 */

public class GlobalUtils {
    public static void onShowAlertDialog(Context c, String title, String msg, final IDialogView dialogView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(c);

        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogView.onDialogSuccess();
                dialog.dismiss();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
