package com.np.pas.e_banquet.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.np.pas.e_banquet.R;
import com.np.pas.e_banquet.ui.adapter.viewHolder.MainViewHolder;
import com.np.pas.e_banquet.ui.vInterface.IMainView;
import com.np.pas.e_banquet.ui.viewModels.BookedTimeViewModel;

/**
 * Created by Rashmi on 11/5/2017.
 */

public class AllTimeRecycleViewAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private String[] mDataset;
    private LayoutInflater layoutInflater;
    private final IMainView iMainView;
    public AllTimeRecycleViewAdapter(Context context,IMainView iMainView,String[]mDataset) {
        this.mDataset = mDataset;
        this.iMainView=iMainView;
        layoutInflater = LayoutInflater.from(context);

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = layoutInflater.inflate(R.layout.activity_cardview_booked, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, int position) {
        MainViewHolder viewHolder = (MainViewHolder) holder;
        BookedTimeViewModel bookedTimeViewModel=new BookedTimeViewModel();
        bookedTimeViewModel.setCustomerName("rashmi");
//        viewHolder.txtCustomerName.setText(bookedTimeViewModel.getCustomerName());
//        viewHolder.txtArrivalTime.setText(bookedTimeViewModel.getArrivalTime());
//        viewHolder.txtDepartureTime.setText(bookedTimeViewModel.getDepartureTime());
        viewHolder.txtCustomerName.setText(mDataset[0]);
        viewHolder.txtArrivalTime.setText(mDataset[1]);
        viewHolder.txtDepartureTime.setText(mDataset[2]);
        viewHolder.txtCustomerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iMainView.onVenueItem();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
