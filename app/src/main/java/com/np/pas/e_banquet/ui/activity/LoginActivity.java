package com.np.pas.e_banquet.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.np.pas.e_banquet.R;
import com.np.pas.e_banquet.presenter.LoginPresenterImpl;
import com.np.pas.e_banquet.presenter.pInterface.LoginPresenter;
import com.np.pas.e_banquet.ui.coreUi.BaseActivity;
import com.np.pas.e_banquet.ui.vInterface.ILoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity<LoginPresenter> implements ILoginView {
    @BindView(R.id.et_login_username)
    EditText etUserName;
    @BindView(R.id.et_login_password)
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        presenter.isUserLogin();
    }

    @Override
    public void setContext() {
        context = this;
    }

    @OnClick(R.id.login)
    public void onLoginClick() {
        presenter.onLoginClick(etUserName.getText().toString(), etPassword.getText().toString());



    }

    @Override
    public void createPresenter() {
        {
            presenter = new LoginPresenterImpl(this);
        }
    }

    @Override
    public void onUserNameError(String msg) {
        etUserName.setError("Please check username");
    }

    @Override
    public void onPasswordError(String msg) {
        etPassword.setError("Please check password");
    }

    @Override
    public void onNavigateToMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}