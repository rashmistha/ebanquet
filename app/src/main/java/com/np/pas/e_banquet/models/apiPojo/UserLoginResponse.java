
package com.np.pas.e_banquet.models.apiPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginResponse {

    @SerializedName("idToken")
    @Expose
    private String idToken;
    @SerializedName("idBranch")
    @Expose
    private Integer idBranch;
    @SerializedName("emailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("employeeName")
    @Expose
    private String employeeName;
    @SerializedName("employeeType")
    @Expose
    private String employeeType;
    @SerializedName("idEmployee")
    @Expose
    private Integer idEmployee;
    @SerializedName("errorMessage")
    @Expose
    private Object errorMessage;
    @SerializedName("errorCode")
    @Expose
    private Boolean errorCode;

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public Integer getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(Integer idBranch) {
        this.idBranch = idBranch;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Boolean errorCode) {
        this.errorCode = errorCode;
    }

}
